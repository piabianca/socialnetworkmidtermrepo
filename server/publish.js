Meteor.publish("posts", function(){
  if(this.userId){
    return PostList.find();
  }
});

Meteor.publish('users', function(){
  if(this.userId) {
    return Meteor.users.find();
  }
});

Meteor.publish('likes', function(){
  if(this.userId) {
    return LikeList.find();
  }
});

Meteor.publish('followers', function(){
    return FollowerList.find({currentUserId: this.userId});
});

Meteor.publish('notifications', function(){
  return NotificationList.find({currentUserId: this.userId});
});
