Meteor.methods({
  'insertNewPost':function(post, createdBy){
    var datePosted = new Date();
    datePosted = moment().format('MMMM Do YYYY, h:mm:ss a');
    PostList.insert({
      post: post,
      createdBy: Meteor.user().username,
      userId: this.userId,
      datePosted: datePosted,
    });
  },
  'insertNewLike':function(postId, userPosted){
    var dateLiked = new Date();
    dateLiked = moment().format('MMMM Do YYYY, h:mm:ss a');
    LikeList.insert({
      post: postId,
      userPosted: userPosted,
      userLiked: this.userId,
      dateLiked: dateLiked
    });
  },
  'insertNewFollower':function(otherUser){
    var dateFollowed = new Date();
    dateFollowed = moment().format('MMMM Do YYYY, h:mm:ss a');
    FollowerList.insert({
      followingId: otherUser,
      currentUserId: this.userId,
      dateFollowed: dateFollowed
    });
  },
  'insertNotif':function(otherUser, action){
    var dateNotified = new Date();
    dateNotified = moment().format('MMMM Do YYYY, h:mm:ss a');
    NotificationList.insert({
      username: Meteor.user().username,
      notifiedUser: otherUser,
      currentUserId: this.userId,
      action: action,
      read: false,
      dateNotified: dateNotified
    });
  },
  'updateRead':function(){
    NotificationList.update({currentUserId: this.userId, read: false},{$set: {read: true}});
  }
});
