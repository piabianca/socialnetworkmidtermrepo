Template.home.helpers({
  'post': function() {
    return PostList.find( {}, {sort:{datePosted:-1}});
  }
});
Template.notification.helpers({
  'count': function() {
    var count = NotificationList.find({currentUserId: Meteor.userId(), read: false}).count();
    return count;
  },
  'notif': function() {
    return NotificationList.find({currentUserId: Meteor.userId(), read: false},{sort: {dateNotified:-1}});
  },
  'message': function() {
    var action = this.action;
    var name = this.username;
    if(action == "like") {
      if(name == Meteor.user().username){
        return "You liked your own status";
      } else {
        return (name + " liked your status");
      }
    }
    else if(action == "follow"){
      return (name + " followed you");
    }
  }
});

Template.navigation.helpers({
  'username': function() {
    var username = Meteor.user().username;
    return username;
  }
});
Template.profile.helpers({
  'post': function() {
    var createdBy = Meteor.user().username;
    return PostList.find({
      createdBy: createdBy
    });
  }
});

Template.users.helpers({
  'user': function() {
    var users = Meteor.users.find({_id: {$not: Meteor.userId()}});
    return users;
  },
  'follow': function() {
    var follow = FollowerList.find({currentUserId: Meteor.userId(), followingId: this._id}).fetch();
    return follow;
  }
});

Template.timeline.helpers({
  'like': function() {
    var like = LikeList.find({post: this._id, userLiked: Meteor.userId()}).fetch();
    return like;
  },
  'count': function() {
    var count = LikeList.find({post: this._id}).count();
    return count;
  }
});
