Meteor.subscribe("posts");
Meteor.subscribe("users");
Meteor.subscribe("likes");
Meteor.subscribe("followers");
Meteor.subscribe("notifications");

Template.register.onCreated(function() {

});

Template.register.onRendered(function() {
  var validator = $('.register').validate({
    submitHandler: function(event) {
      var username = $('[name=username]').val();
      var password = $('[name=password]').val();
      Accounts.createUser({
        username: username,
        password: password
      }, function(error) {
        if (error) {
          if (error.reason == "Username already exists.") {
            validator.showErrors({
              username: "That username already belongs to a registered user."
            });
          }
        } else {
          Router.go("home");
        }
      });
    }
  });
});

Template.register.onDestroyed(function() {

});

Template.login.onCreated(function() {

});

Template.login.onRendered(function() {
  var validator = $('.login').validate({
    submitHandler: function(event) {
      var username = $('[name=username]').val();
      var password = $('[name=password]').val();
      Meteor.loginWithPassword(username, password, function(error) {
        if (error) {
          if (error.reason == "User not found") {
            validator.showErrors({
              username: error.reason
            });
          }
          if (error.reason == "Incorrect password") {
            validator.showErrors({
              password: error.reason
            });
          }
        } else {
          var currentRoute = Router.current().route.getName();
          if (currentRoute == "login") {
            Router.go("home");
          }
        }
      });
      $('[name="username"]').val('');
      $('[name="password"]').val('');
      Router.go('home');
    }

  });
});

Template.login.onDestroyed(function() {

});

Template.status.onCreated(function() {

});

Template.status.onRendered(function() {
  $('.post').validate({
    submitHandler: function(event) {
      var post = $('[name=post]').val();
      Meteor.call('insertNewPost', post);
      $('[name="post"]').val('');
    }
  });
});

Template.status.onDestroyed(function() {

});
