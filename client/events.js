Template.navigation.events({
  'click .logout': function(event) {
    event.preventDefault();
    Meteor.logout();
    Router.go('login');
  },
  'click .notif':function(event) {
    Meteor.call('updateRead');
  }
});

Template.timeline.events({
  'click .like': function(event) {
    event.preventDefault();
    var postId = this._id;
    var otherUser = this.UserId;
    var action = "like";
    if (LikeList.findOne({post: postId, userLiked: Meteor.userId()})) {
      console.log('Already liked');
    } else {
      Meteor.call('insertNewLike', postId, otherUser);
      Meteor.call('insertNotif', otherUser, action);
    }
  }
});

Template.users.events({
  'click .follow': function(event) {
    event.preventDefault();
    var otherUser = this._id;
    var action = "follow";
    if (FollowerList.findOne({followingId: otherUser, currentUserId: Meteor.userId()})) {
      console.log('Already followed');
    } else {
      Meteor.call('insertNewFollower', otherUser);
      Meteor.call('insertNotif', otherUser, action);
    }
  }
});
