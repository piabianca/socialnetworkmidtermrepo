$.validator.setDefaults({
  rules: {
    password: {
      minlength: 6,
      whitespace: true
    },
    username: {
      maxlength: 8,
      whitespace: true,
    }
  }
});

jQuery.validator.addMethod("whitespace", function(value) {
  return value.indexOf(" ") < 0;
}, "Whitespace is not allowed");
