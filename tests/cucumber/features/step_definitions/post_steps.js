(function() {
  'use strict';
   var _ = require('underscore');

   module.exports = function() {
     var url = require('url');

     this.Given(/^I am logged in$/, function(){
       this.server.call('reset');
       this.server.call('userPillow');
     });

     this.When(/^I enter my status "([^"]*)" in "([^"]*)"$/, function(status, location) {
       this.client.waitForExist(location);
       this.client.setValue(location, status);
     });
   };
})();
