(function() {
  'use strict';

  var _ = require('underscore');

  module.exports = function() {
    var url = require('url');

    this.Given(/^I am a new user$/, function() {
      this.server.call('reset');
    });

    this.Given(/^I am a registered user$/, function() {
      this.server.call('reset');
      this.server.call('userPillow');
      this.server.call('userFace');
    });

    this.When(/^I enter my username "([^"]*)" and my password "([^"]*)"$/, function(username, password) {
      this.client.waitForExist('#inputUsername');
      this.client.waitForExist('#inputPassword');
      this.client.setValue('#inputUsername', username);
      this.client.setValue('#inputPassword', password);
      this.client.pause(5000);
    });

    this.When(/^I click "([^"]*)"$/, function(object) {
      this.client.waitForExist(object);
      this.client.click(object);
      this.client.pause(5000);
    });

    this.Then(/^I should see the text "([^"]*)" in "([^"]*)"$/, function(text, location){
      this.client.waitForExist(location);
      this.client.getText(location).should.include(text);
      this.client.pause(5000);
    });
  };
})();
