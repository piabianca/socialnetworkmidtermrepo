(function() {

  'use strict';

  // You can include npm dependencies for support files in tests/cucumber/package.json
  var _ = require('underscore');

  module.exports = function() {

    // You can use normal require here, cucumber is NOT run in a Meteor context (by design)
    var url = require('url');

    this.Given(/^I am a new user$/, function() {
      this.server.call('reset');
    });

    this.When(/^I navigate to "([^"]*)"$/, function(relativePath) {
      this.client.url(url.resolve(process.env.ROOT_URL, relativePath));
      this.client.pause(10000);
    });

    this.Then(/^I should see "([^"]*)"$/, function(object) {
      this.client.waitForExist(object);
      this.client.pause(10000);
    });
  };
})();
