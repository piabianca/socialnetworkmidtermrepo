Feature: User register

  As a new user
  I want to be able to register
  So that I can access the app

  Background:
    Given I am a new user

  @dev
  Scenario: A user will register a new account
    When I navigate to "/register"
    When I enter my username "pia" and my password "123456"
    When I click "#btnRegister"
    Then I should see "#timeline"

  @dev
  Scenario: A user will register a username more than 8 characters
    When I navigate to "/register"
    When I enter my username "piabianca" and my password "123456"
    When I click "#btnRegister"
    Then I should see the text "Please enter no more than 8 characters." in "#inputUsername-error"

  @dev
  Scenario: A user will register a username with whitespace
    When I navigate to "/register"
    When I enter my username "bia nca" and my password "123456"
    When I click "#btnRegister"
    Then I should see the text "Whitespace is not allowed" in "#inputUsername-error"

  @dev
  Scenario: A user will register a password less than 6 characters
    When I navigate to "/register"
    When I enter my username "pikachu" and my password "12345"
    When I click "#btnRegister"
    Then I should see the text "Please enter at least 6 characters." in "#inputPassword-error"

  @dev
  Scenario: A user will register a password with whitespace
    When I navigate to "/register"
    When I enter my username "pikachu" and my password " 12345"
    When I click "#btnRegister"
    Then I should see the text "Whitespace is not allowed" in "#inputPassword-error"

  Background:
    Given I am a registered user
  @ignore
  Scenario: A user will register an existing account
    When I navigate to "/register"
    When I enter my username "pillow" and my password "123456"
    When I click "#btnRegister"
    Then I should see the text "That username already belongs to a registered user." in "#inputUsername-error"
