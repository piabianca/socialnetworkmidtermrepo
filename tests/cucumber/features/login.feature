Feature: User login

  As a new user
  I want to be able to login
  So that I can access the app

  Background:
    Given I am a registered user

  @dev
  Scenario: User login a registered username and password
    When I navigate to "/login"
    When I enter my username "pillow" and my password "123456"
    When I click "#btnLogin"
    Then I should see "#timeline"

  @dev
  Scenario: User login a valid username with no password
    When I navigate to "/login"
    When I enter my username "pillow" and my password ""
    When I click "#btnLogin"
    Then I should see the text "This field is required." in "#inputPassword-error"

  @dev
  Scenario: User login with no username
    When I navigate to "/login"
    When I enter my username "" and my password "123456"
    When I click "#btnLogin"
    Then I should see the text "This field is required." in "#inputUsername-error"

  @ignore
  Scenario: User login a wrong username
    When I navigate to "/login"
    When I enter my username "pikku" and my password "123456"
    When I click "#btnLogin"
    Then I should see the text "User not found" in "#inputUsername-error"
