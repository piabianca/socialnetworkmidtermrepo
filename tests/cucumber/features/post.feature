Feature: User post

  As a user
  I want to be able to post
  So that I can share my thoughts

Background:
  Given I am a registered user

@dev
Scenario: User will post a status
  When I navigate to "/login"
  When I enter my username "pillow" and my password "123456"
  When I click "#btnLogin"
  When I enter my status "Heloo!" in "#postTextarea"
  When I click "#btnPost"
  Then I should see the text "Heloo!" in "#timeline"

@dev
Scenario: User will post in the profile page
  When I navigate to "/login"
  When I enter my username "pillow" and my password "123456"
  When I click "#btnLogin"
  When I navigate to "/profile"
  When I enter my status "Heloo!" in "#postTextarea"
  When I click "#btnPost"
  Then I should see the text "Heloo!" in "#timeline"
