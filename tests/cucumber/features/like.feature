Feature: User likes the post of other user

As a user
I want to be able to like the post of other users
So that they will know that I saw their posts

Background:
  Given I am a registered user

@dev
Scenario: User will post a status and another user will like the status
    When I navigate to "/login"
    When I enter my username "pillow" and my password "123456"
    When I click "#btnLogin"
    When I enter my status "Heloo!" in "#postTextarea"
    When I click "#btnPost"
    When I should see the text "Heloo!" in "#timeline"
    When I click "#btnLogout"
    When I navigate to "/login"
    When I enter my username "face" and my password "123456"
    When I click "#btnLogin"
    When I should see the text "Heloo!" in "#timeline"
    When I click "#like"
    Then I should see the text "you have liked this" in "#status"
