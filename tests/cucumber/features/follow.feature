Feature: User follows other user

As a user
I want to be able to follow other users
So that I can view their posts

Background:
  Given I am a registered user

@dev
Scenario: User will follow other user
    When I navigate to "/login"
    When I enter my username "pillow" and my password "123456"
    When I click "#btnLogin"
    When I should see the text "Logout" in "#btnLogout"
    When I click "#btnLogout"
    When I navigate to "/login"
    When I enter my username "face" and my password "123456"
    When I click "#btnLogin"
    When I navigate to "/users"
    When I click "#follow"
    Then I should see the text "Followed" in "#followed"
