(function () {

  'use strict';

  Meteor.methods({
    'reset' : function() {
       Meteor.users.remove({});
       PostList.remove({});
    },
    'userPillow' : function() {
      Accounts.createUser ({
        username: "pillow",
        password: "123456"
      });
    },
    'userFace' : function() {
      Accounts.createUser ({
        username: "face",
        password: "123456"
      });
    }
  });
})();
